import {combineReducers} from 'redux';
import {petReducer} from './petReducer';

const appReducer = combineReducers({
    pet:petReducer
})

const rootReducer = (state, action) => {
    return appReducer(state, action);
}

export default rootReducer;