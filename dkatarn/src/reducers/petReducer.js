const initialState={
    loading: false,
    pets:[],
    error: null
}

const petReducer=(state=initialState, action) => {
    switch (action.type){
        case 'GET_PET_LIST_STARTED':
            return{
                ...state,
                loading: true
            }
            case 'GET_PET_LIST_SUCCESS':
                return {
                    ...state,
                    error: null,
                    loading: false,
                    pets: action.payload
                }
                case 'GET_PET_LIST_FAILURE' :
                    return {
                        ...state,
                        loading: false,
                        error: action.payload.error
                    }
                default :
                 return state
    }
}

export {petReducer}
