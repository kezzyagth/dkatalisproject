import axios from 'axios';

const getPetListStarted = () => {
    return{
        type: 'GET_PET_LIST_STARTED',       
    }
}

const getPetListFailure = error => {
    return{
        type: 'GET_PET_LIST_FAILURE',
        payload: {
            error
        }
    }
}

const getPetListSuccess = pets => {
    return{
        type: 'GET_PET_LIST_SUCCESS',
        payload: {
            pets
        }
    }
}

export const getPetList = () => {
    return dispatch => {
        dispatch(getPetListStarted())
        axios.get(`http://20.4.12.213:3000/pets`, {
            auth: {
                username: 'john',
                password: 'secret'
            }
        })
        .then(res => {
            dispatch(getPetListSuccess(res.data))
        }).catch(err => {
            dispatch(getPetListFailure(err))
        })

    }

}