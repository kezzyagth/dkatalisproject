import {StyleSheet} from 'react-native';

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff6666',
  },
  nameText: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#b30000',
    textAlign: 'center',
    marginBottom: 10,
  },
  contText: {
    fontSize: 15,
    color: '#001f4d',
  },
  text: {
    color: '#b30000',
    fontSize: 13,
  },
  image: {
    width: '100%',
    height: 400,
  },
  txtHeader: {
    fontSize: 40,
    textAlign: 'center',
    margin: 10,
    color: 'white',
  },
  header: {
    height: 75,
    backgroundColor: '#ff6666',
    width: 900,
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 3,
    borderColor: '#b30000',
    backgroundColor: '#ffcccc',
  },
  data: {
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 3,
    borderColor: '#b30000',
    backgroundColor: '#ffcccc',
  },
});

export default style;
