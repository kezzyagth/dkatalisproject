import React, {Component} from 'react';
import {Text, View, FlatList, TouchableOpacity} from 'react-native';
import axios from 'axios';
import {NavigationEvents} from 'react-navigation';
import style from '../styles/reactStylecss';
import getPetList from '../action/index'
import {connect} from 'react-redux';

class ListPets extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      pets: [],
    };
  }
  componentDidMount() {
    axios
      .get('http://20.4.12.213:3000/pets', {
        auth: {
          username: 'john',
          password: 'secret',
        },
      })
      .then(res => {
        const pets = res.data;
        this.setState({
          pets,
          isLoading: false,
        });
      });
  }

  renderItem = pet => {
    return (
      <View style={style.list}>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('DetailList', pet.item)
          }>
          <Text style={style.nameText}>{pet.item.name}</Text>
          <Text style={style.text}>Breed : {pet.item.breed}</Text>
          <Text style={style.text}>Colour : {pet.item.colour}</Text>
          <Text style={style.text}>Age : {pet.item.age}</Text>
          <Text style={style.text}>Next Checkup : {pet.item.next_checkup}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={style.container}>
        <NavigationEvents
          onDidFocus={payload => console.log('did focus', payload)}
        />
        <View style={style.header}>
          <Text style={style.txtHeader}> List Pets </Text>
        </View>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('DetailList')}
        />
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderItem}
          data={this.state.pets}
        />
      </View>
    );
  }
}

const mapStateToProps= state =>{
  return{
    pet: state.pet
  }
}

export default connect( mapStateToProps,{getPetList}) (ListPets)