/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';

class ImagePet extends Component {
  render() {
    return (
      <View>
        <View>
          <Image
            style={{width: '100%', height: 400}}
            source={{
              uri:
                'https://static.boredpanda.com/blog/wp-content/uploads/2017/03/Im-a-professional-dog-photographer-58bf69e8072b3__880.jpg',
            }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginTop: 20,
          }}>
          <View>
            <Text style={{fontSize: 15}}>Name : Bruno </Text>
            <Text style={{fontSize: 15}}>Breed : Golden retriver </Text>
            <Text style={{fontSize: 15}}>Next_checkup : 09-12-2019 </Text>
          </View>
          <View>
            <Text style={{fontSize: 15}}> Age : 5 </Text>
            <Text style={{fontSize: 15}}> Colour : Brown </Text>
          </View>
        </View>
      </View>
    );
  }
}
export default ImagePet;
