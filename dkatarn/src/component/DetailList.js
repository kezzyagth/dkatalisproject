/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable no-unreachable */
/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import style from '../styles/reactStylecss';
import axios from 'axios';
export default class DetailList extends Component {
  static navigationOptions = {
    title: `Pet Details`,
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      pet: '',
    };
  }
  componentDidMount() {
    const {getParam} = this.props.navigation;
    this.setState({
      pet: {
        id: getParam('id'),
        name: getParam('name'),
        breed: getParam('breed'),
        next_checkup: getParam('next_checkup'),
        age: getParam('age'),
        colour: getParam('colour'),
        vaccinations: getParam('vaccinations'),
        imageUri: getParam('imageUri'),
      },
      updateNextCheckUp: false,
      updateAge: false,
      addVaccinations: false,
      updateImageUri: false,
    });
  }
  updateNextCheckup() {
    if (this.state.updateNextCheckUp) {
      return (
        <DatePicker
          style={{width: '100%', padding: 10}}
          date={this.state.pet.next_checkup}
          mode="date"
          format="YYYY-MM-DD"
          minDate={new Date().toISOString()}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
          onDateChange={date => {
            this.setState({pet: {...this.state.pet, next_checkup: date}});
          }}
        />
      );
    }
    return;
  }
  updateAge() {
    if (this.state.updateAge) {
      return (
        <TextInput
          keyboardType="numeric"
          value={this.state.pet.age.toString()}
          onChangeText={text =>
            this.setState({pet: {...this.state.pet, age: text}})
          }
        />
      );
      return;
    }
  }
  updateVaccinations(item, index) {
    if (this.state[`updateVaccinations${index}`]) {
      let temp = this.state.pet.vaccinations;
      return (
        <TextInput
          value={this.state.pet.vaccinations[index]}
          onChangeText={text => {
            temp[index] = text;
            this.setState({pet: {...this.state.pet, vaccinations: temp}});
          }}
          onEndEditing={e => {
            this.setState({
              [`updateVaccinations${index}`]: !this.state[
                `updateVaccinations${index}`
              ],
            });
          }}
        />
      );
    }
    return (
      <Text
        onPress={() =>
          this.setState({
            [`updateVaccinations${index}`]: !this.state[
              `updateVaccinations${index}`
            ],
          })
        }>
        {item}
      </Text>
    );
  }
  addVaccinations() {
    if (this.state.addVaccinations) {
      let temp = this.state.pet.vaccinations;
      return (
        <TextInput
          style={{paddingLeft: 10}}
          placeholder="Input the new vaccination"
          onEndEditing={e => {
            temp.push(e.nativeEvent.text);
            this.setState({
              addVaccinations: !this.state.addVaccinations,
              pet: {...this.state.pet, vaccinations: temp},
            });
          }}
        />
      );
    }
    return;
  }
  updateImageUri() {
    if (this.state.updateImageUri) {
      return (
        <TextInput
          value={this.state.pet.imageUri}
          onChangeText={text => {
            this.setState({pet: {...this.state.pet, imageUri: text}});
          }}
          onEndEditing={e => {
            this.setState({updateImageUri: !this.state.updateImageUri});
          }}
        />
      );
    }
    return;
  }
  render() {
    const options = {
      title: 'Select Avatar',
      customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        this.setState({
          avatarSource: source,
        });
      }
    });
    return (
      <View style={style.container}>
        <Image source={this.state.avatarSource} style={style.image} />
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() =>
              this.setState({updateImageUri: !this.state.updateImageUri})
            }>
            <Image
              source={{uri: this.state.pet.imageUri}}
              style={{flex: 1}}
              resizeMode="contain"
            />
          </TouchableOpacity>
          {this.updateImageUri()}
        </View>
        <View style={style.data}>
          <View style={{flex: 1}}>
            <Text>Name : {this.state.pet.name}</Text>
            <Text>Breed : {this.state.pet.breed}</Text>
            <Text
              onPress={() =>
                this.setState({
                  updateNextCheckUp: !this.state.updateNextCheckUp,
                })
              }>
              Next_Checkup : {this.state.pet.next_checkup}
            </Text>
            {this.updateNextCheckup()}
            <Text
              onPress={() => this.setState({updateAge: !this.state.updateAge})}>
              Age : {this.state.pet.age}
            </Text>
            {this.updateAge()}
            <Text>Colour : {this.state.pet.colour}</Text>
          </View>
          <View style={{flex: 1, paddingLeft: 30}}>
            <Text
              onPress={() =>
                this.setState({addVaccinations: !this.state.addVaccinations})
              }>
              Vaccinations :
            </Text>
            {this.addVaccinations()}
            <FlatList
              style={{paddingLeft: 30}}
              data={this.state.pet.vaccinations}
              keyExtractor={({index}) => index}
              renderItem={({item, index}) =>
                this.updateVaccinations(item, index)
              }
            />
          </View>
        </View>
      </View>
    );
  }
  async componentWillUnmount() {
    await axios.put(
      `http://20.4.12.223:3000/pets/${this.state.pet.id}`,
      this.state.pet,
      {
        auth: {
          username: 'john',
          password:
            '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm', // 'secret'
        },
      },
    );
  }
}
