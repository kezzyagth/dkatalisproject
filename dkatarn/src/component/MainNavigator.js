import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import DetailList from './DetailList';
import ListPets from './ListPets';

const MainNavigator = createStackNavigator(
  {
    ListPets: {screen: ListPets},
    DetailList: {screen: DetailList},
  },
  {
    initialRouteName: 'ListPets',
  },
);

const Container = createAppContainer(MainNavigator);

export default Container;
