const hapi = require ('@hapi/hapi')
const Joi = require('@hapi/joi');
const Path= require('path')
const Inert = require('@hapi/inert');
const pets = require('./pets.json')
const Bcrypt = require('bcrypt');
pets.forEach(element => {
    Object.assign(element,{sold:false})
});
const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret' 
        name: 'John Doe',
        id: '2133d32a'
    }
};
const validate = async (request, username, password) => {
    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };
    return { isValid, credentials };
};
const init = async()=>{
    const server= hapi.server({
        port: 4000,
        host: 'localhost', 
        routes:{
            files:{
                relativeTo: Path.join(__dirname)
            }
        }
    })
    await server.register(require('@hapi/basic'));
    server.auth.strategy('simple', 'basic', { validate });
    await server.register(Inert)
    //get one id in file pets.json
    server.route({
        method: 'GET',
        path: '/pet/{id}',
        handler: function (request, h) {
            return pets.filter( pet => pet.id == request.params.id);
        },
        options:{
            auth: 'simple',
            validate:{
                params:{
                    id: Joi.number().integer()
                }
            } 
        }
    })
    //server to get file pets.json
    server.route({
        method: 'GET',
        path: '/pets',
        handler: function (request, h) {
            if(!request.query.sort && !request.query.limit && !request.query.offset && !request.query.filter){
                return h.response(pets.filter(pet=>pet.sold==false)).code(200)
            }
            const  sort = request.query.sort
            const offset = parseInt(request.query.offset)
            const limit = parseInt(request.query.limit)
            const filter = request.query.filter
            if(filter=="all"){
                return pets
            } else if (filter=="unsold"){
                return pets.filter(pet=>pet.sold==false)
            } else if(filter=="sold"){
                return pets.filter(pet=>pet.sold==true)
            }
            let result
            if(typeof pets[0][sort]== 'string'){
                result=pets.sort((a,b)=>a[sort]>b[sort])
            }  else if(typeof pets[0][sort] == 'number') { 
                result=pets.sort((a,b)=>a[sort]-b[sort])
            }
            return result.slice(offset,limit+offset+filter)
        },
        options:{
            auth: 'simple',
            validate:{
                query:{
                    sort: Joi.string(),
                    limit: Joi.number().integer().min(0),
                    offset: Joi.number().integer().min(0),
                    filter: Joi.string()
                }
            }
        }
    })
    server.route({
        method: 'POST',
        path: '/pets',
        handler: function(request, h){
            request.payload["id"] = Math.max(...pets.map(eachpet => eachpet.id))+1
            pets.push(request.payload)
            return h.response(request.payload).code(201)
        },
        options:{
            auth: 'simple',
            validate:{
                payload:{
                    id: Joi.forbidden(),
                    name: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    breed: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    colour: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    age: Joi.number(),
                    next_checkup: Joi.date(),
                    vaccination: Joi.array()
                }
            }
        }
    })
    server.route({
        method: 'PUT',
        path: '/pets/{id}',
        handler: function(request, h){
            let id= request.params.id
            let indexFound = pets.findIndex(pet=>pet.id==id)
            let name= pets[indexFound].name
            if(request.payload.name!=name){
                return h.response({
                    statusCode: 409,
                    error: "Conflict",
                    message: "Name is conflict"
                }).code(409)
            }
            let petUpdate
            if(!request.payload.id){
                petUpdate= Object.assign({id:id}, request.payload)
            } else {
                let thisID= pets[indexFound].id
                if(request.payload.id!=thisID){
                    return h.response({
                        statusCode: 409,
                        error: "Conflict",
                        message: "Name is conflict"
                    }).code(409)
                }
                petUpdate= request.payload 
            }
            pets[indexFound]=petUpdate
            return h.response(petUpdate).code(202)
        },
        options:{
            auth: 'simple',
            validate:{
                payload:{
                    id: Joi.number().integer(),
                    name: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    breed: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    colour: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    age: Joi.number(),
                    next_checkup: Joi.date(),
                    vaccinations: Joi.array()
                }
            }
        }  
    })
    server.route({
        method: 'DELETE',
        path: '/pets/{id}',
        handler: function (request, h) {
            let id= request.params.id
            Object.assign(pets.find(pet=> pet.id==id), {sold:true})
            return h.response({
                statusCode: 202,
                message: "Delete success"
            });
        },
        options: {
            auth: 'simple'
        },
    })
        process.on('unhandledRejection', (err) => {
        console.log(err)
        process.exit(1)
    })
     await server.start()
    console.log('Server running on %s', server.info.uri)
    //untuk tes yg ini dijalankan yg init() di comment
    return server
}

init();