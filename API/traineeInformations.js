const Hapi = require('@hapi/hapi');
const {trainee} = require('../project2/profileClass')
const eci = new trainee('Eci','Suci','F','20','nursing','EciHouse'); //place the object on the outside so the others can use the object too
const shanon = new trainee ('shanon','athooya','F','18','coding','rumahShanon');
const trainees = [eci,shanon];

const traineeTrainer = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: (request,h) =>{
            return 'Hii, '
        }
    })

    server.route({
        method: 'GET',
        path: '/{name}',
        handler: (request, h) => {
            return `Hi ${request.params.name}` ;
        }
    })

    server.route({
        method: 'GET',
        path: '/trainee',
        handler: (request,h) =>{
            return trainees
        }
    })
    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
})

init()

