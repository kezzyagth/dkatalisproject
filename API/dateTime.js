const Hapi = require('@hapi/hapi');

const init = async () => {
    
    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });
    
    server.route({
        method: 'GET',
        path: '/api/parsetime',
        handler: (request,h) =>{
            const date = new Date(request.query.iso)
            return {'hour' : date.getHours(),
                   'minute ': date.getMinutes(),
                   'seconds ': date.getSeconds()
        }}
    })

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});
init();