'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { start } = require('../authentication');

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await start();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds statusCode for path "/" is 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/',
            headers:{'Authorization':`Basic ${btoa('secret')}`}
        });
        expect(res.statusCode).to.equal(200);
    });
});