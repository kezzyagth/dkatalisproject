const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {start}=require('../server')
describe('GET',()=>{
    let server
    beforeEach(async ()=>{
        server=await start()
    })
    afterEach(async ()=>{
        await server.stop()
    })
    it('respond with 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.statusCode).to.equal(200)
    })
    it('response path "/" return "This is a root route"',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.result).to.equal("This is a root route")
    })
    it('response path "/kezia" return "Hi kezia"',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/kezia'
        })
        expect(res.result).to.equal('Hi kezia')
    })
    it('response path "/trainee" return array about trainee',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/trainee'
        })
        expect(res.result).to.equal(
                {
                  "name": "Eci Suci",
                  "age": 20,
                  "gender": "F",
                  "interest": "nursing",
                  "address": "EciHouse"
                }    
        )
    })
})
