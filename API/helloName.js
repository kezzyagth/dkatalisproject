const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route({
        method: 'GET',
        path: '/{user}',
        handler: (request, h) => {
            return `Hello ${request.params.user}!`;
        },
        options : {
            validate : {
                params : {
                    user : Joi.string().min(3).regex(/^[A-Za-z]+$/)
                }
            }
        }
    })

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});
init();