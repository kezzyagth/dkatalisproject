'use strict'
const Hapi = require('@hapi/hapi');
const pets = require('./pets.json');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/pets', { useNewUrlParser: true ,useUnifiedTopology: true});

const server = Hapi.server({
    port: 3000,
    host: 'localhost'
});

const Pet = mongoose.model('Pet',{
    id: Number,
    name: String,
    breed: String,
    colour: String,
    age: Number,
    next_checkup: Date,
    vaccinations: Array
})

const mySort = {id:1};

pets.forEach(pet => {
    Object.assign(pet,{sold:false})
});

server.route([{
  method: 'GET',
  path: '/',
  handler: function () {
      return 'Hello World!';
  }
},{
    method: 'GET',
    path: '/allpets',
    handler: function () {
        return Pet.find().sort(mySort);
    }
  },{
    method: 'GET',
    path: '/pets/{id}',
    handler: function (request, h) {
        const petFiltered =pets.filter(pet => pet.sold=false)
        return pets.filter( pet => pet.id == request.params.id)
    }
},{
    method: 'POST',
    path: '/pets',
    handler: (request, h) =>{
        const petFiltered =pets.filter(pet => pet.sold=false)
        let id = Math.max(...pets.map(pet => pet.id))+1
        let newinput = Object.assign({id},request.payload)
        return pets.push(newinput)
    }
},{
    method: 'GET',
    path: '/pets',
    handler: function (request, h) {
        const petFiltered =pets.filter(pet => pet.sold=false)
        const sort = request.query.sort
        const offset = parseInt(request.query.offset)
        const limit = parseInt(request.query.limit)
        const filter = request.query.filter="sold"
        let result
        if(typeof pets[0][sort]== 'string'){
            result=petFiltered.sort((a,b)=>a[sort]>b[sort])
        }  else if(typeof pets[0][sort] == 'number') { 
            result=petFiltered.sort((a,b)=>a[sort]-b[sort])
        }
        return result.slice(offset,limit+offset)
    }},{
        method: 'PUT',
        path: '/pets/{id}',
        handler: function(request, h){
            const petFiltered =pets.filter(pet => pet.sold=false)
            let id= request.params.id
            let indexFound = pets.findIndex(pet=>pet.id==id)
            let name= pets[indexFound].name
            if(request.payload.name!=name){
                return h.response({
                    statusCode: 409,
                    error: "Conflict",
                    message: "Name is conflict"
                }).code(409)
            }
            let petUpdate
            if(!request.payload.id){
                petUpdate= Object.assign({id:id}, request.payload)
            } else {
                let thisID= pets[indexFound].id
                if(request.payload.id!=thisID){
                    return h.response({
                        statusCode: 409,
                        error: "Conflict",
                        message: "Name is conflict"
                    }).code(409)
                }
                petUpdate= request.payload
            }
            pets[indexFound]=petUpdate
            return h.response(petUpdate).code(202)
        }
    },
    {
        method: 'DELETE',
        path: '/pets/{id}',
        handler: function (request, h) {
            let id= request.params.id
            Object.assign(pets.find(pet=> pet.id==id), {sold:true})
            return h.response({
                statusCode: 202,
                message: "Delete success"
            });
        }
    }
    ]);

exports.init = async () => {
    await server.initialize();
    return server;
};

exports.start = async () => {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});