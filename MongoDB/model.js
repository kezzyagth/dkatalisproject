let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/pets', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('debug', true);
let Schema = mongoose.Schema;

const petSchema = new Schema(
    {
        id: Number,
        name: String,
        breed: String,
        colour: String,
        age: Number,
        next_checkup: Date,
        vaccinations: Array,
        sold : Boolean
    }
)


petSchema.pre('validate', function(next) {
  // do stuff
  console.log('trying to validate a new pet1')
  return next()
  console.log('trying to validate a new pet3') // this line will not be executed
});
petSchema.pre('validate', function(next) {
  // do stuff
  console.log('trying to validate a new pet2')
  next()
});
petSchema.post('validate', function(next) {
  // do stuff
  console.log('a new pet has been validated')
  //next()
});
petSchema.pre('save', function(next) {
    // do stuff
    console.log('trying to save a new pet')
    next()
});
petSchema.post('save', function(doc) {
    // do stuff
    console.log('a new pet has been saved '+ doc.name)
    //next()
});

/* petSchema.pre('save', function(next) {
    const err = new Error('something went wrong');
    // If you call `next()` with an argument, that argument is assumed to be
    // an error.
    next(err);
  });
petSchema.pre('save', function() {
    // You can also return a promise that rejects
    return new Promise((resolve, reject) => {
      reject(new Error('something went wrong'));
    });
  });
petSchema.pre('save', function() {
    // You can also throw a synchronous error
    throw new Error('something went wrong');
  }); */

/* // Takes 2 parameters: this is an asynchronous post hook
petSchema.post('save', function(doc, next) {
  setTimeout(function() {
    console.log('name');
    // Kick off the second post hook
    next();
  }, 10);
});

// Will not execute until the first middleware calls `next()`
petSchema.post('save', function(doc, next) {
  console.log('breed');
  //next();
});
 */

const Pet = mongoose.model('pets', petSchema);

/* const schema = new mongoose.Schema({ name: String });
// Mongoose will call this middleware function, because this script adds
// the middleware to the schema before compiling the model.
petSchema.pre('save', () => console.log('Hello from pre save'));

// Compile a model from the schema
const Pet = mongoose.model('Pet', schema);

new Pet({ name: 'test' }).save(); */
module.exports = { Pet, petSchema }