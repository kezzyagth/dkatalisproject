const pets = require('./pets.json')
const { Pet, petSchema } = require('./model')

const getAllPets = function (request, h) {
    request.log('error', 'Event error');
    logging(request, h) //get the parameters
    return Pet.find()
}

const getPetById = function (request, h) {
    return Pet.findOne({ id: request.params.id })
}

const getPetByQuery = async function (request, h) {
    const query = request.query
    if (!query.sort && !query.limit && !query.offset && !query.filter) {
        return Pet.find({}, { _id: 0, __v: 0, sold: 0 }, (err, res) => {
            if (err) {
                return h.response(err).code(500)
            } else {
                return h.response(res).code(200)
            }
        })
    } else {
        let sort = query.sort ? query.sort : { 'sold': 'name' }
        let offset = query.offset ? parseInt(query.offset) : 0
        let limit = query.limit ? parseInt(query.limit) : 10
        let filter = query.filter ? query.filter : { sold: false }
        console.log(filter)
        const result = await Pet.find({ sold: false }, { _id: 0, __v: 0 })
            .sort({ [sort]: 1 })
            .skip(offset)
            .limit(limit)
            .lean()
        return h.response(result).code(200)
    }
}

const postPetHandler = async (request, h) => {
    try {
        let max = await Pet.aggregate([{ $group: { _id: 'max', maxId: { $max: "$id" } } }])
        request.payload.id = max[0].maxId + 1
        let animal = new Pet(request.payload);
        let result = await animal.save();
        return h.response(result);
    } catch (error) {
        return h.response(error.message).code(500);
    }
}

const updatePetHandler = async function (request, h) {
    try {
        var result = await Pet.findOneAndUpdate(request.params.id, request.payload, { new: true });
        return h.response(result);
    } catch (error) {
        return h.response(error).code(500);
    }
}

const deletePetHandler = async (request, h) => {
    try {
        var result = await Pet.findOneAndUpdate(request.params.id, { sold: true });
        return h.response(result);
    } catch (error) {
        return h.response(error).code(500);
    }
}

const logging = async (request, h) =>{
    // request.log is HAPI standard way of logging
    request.log(['a', 'b'], 'Request into Pets')
 
    // you can also use a pino instance, which will be faster
    request.logger.info('In handler %s', request.path)

    return 'hello Pets'
}

const getPet = async (filter) => {
    return await Pet.findOne({id: filter}).lean()
}

const getPetHandler = async function (request, h) {
    const filter = request.params.id
    await request.server.method.getPet.cache.drop(filter)
    const rc = request.server.method.getPet(filter)

    return rc;
}

module.exports = { getAllPets, getPetById, getPetByQuery, postPetHandler, updatePetHandler, deletePetHandler, getPet}