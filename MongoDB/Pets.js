const Path = require('path');
const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi')
const Bcrypt = require('bcrypt');
const CatboxRedis = require('@hapi/catbox-redis');
const { getAllPets, getPetById, getPetByQuery, postPetHandler, updatePetHandler, deletePetHandler, getPet} = require('./Handler')

const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const server = Hapi.server({
    port: 3000,
    host: '0.0.0.0',
    cache:[
            {
                name: 'my_cache',
                provider: {
                    constructor: CatboxRedis,
                    options: {
                        partition : 'my_cached_data',
                        host: 'localhost',
                        port: 6379,
                        database: 0
                    }
                }
            }
        ]
});

exports.init = async () => {

    await server.initialize();
    return server;
};

exports.start = async () => {
    await server.register(require('@hapi/basic'));
    await server.register({
        plugin: require('hapi-pino'),
        options: {
          prettyPrint: process.env.NODE_ENV !== 'production',
          stream:'./logs.log',
          // Redact Authorization headers, see https://getpino.io/#/docs/redaction
          redact: ['req.headers.authorization']
        }
    })

    server.auth.strategy('simple', 'basic', { validate });

    server.route({
        method: 'GET',
        path: '/pets', //open localhost : http://localhost:3000/pets
        handler: getAllPets, 
        options: {
            auth: 'simple',
            log:{
                collect: true
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/pets/{id}', //open localhost : http://localhost:3000/pets/id
        options: {
            validate: {
                params: {
                    id: Joi.number().min(1).integer()
                }
            },
            auth: 'simple'
        },
        handler: getPetById,
    })

       server.route({
        method: 'GET', //http://localhost:3000/pets/query?sort=id&limit=2&offset=2&filter=all
        path: '/pets/query',
        handler: getPetByQuery
    })

    server.route({
        method: "POST",
        path: "/pets",
        options: {
            validate: {
                payload: {
                    id: Joi.number().integer(),
                    name: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    breed: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    colour: Joi.string().regex(/^[A-Za-z\s]+$/).required(),
                    age: Joi.number(),
                    next_checkup: Joi.date(),
                    vaccinations: Joi.array()
                }
            },
            auth: 'simple'
        },
        handler: postPetHandler
    });

    server.route({
        method: 'PUT',
        path: '/pets/{id}',
        options: {
            validate: {
                params: {
                    id: Joi.required(),
                    payload: {
                        name: Joi.string().regex(/^[A-Za-z\s]+$/),
                        breed: Joi.string().regex(/^[A-Za-z\s]+$/),
                        colour: Joi.string().regex(/^[A-Za-z\s]+$/),
                        age: Joi.number(),
                        next_checkup: Joi.date(),
                        vaccinations: Joi.array()
                    }
                }
            },
            auth: 'simple'
        },
        handler: updatePetHandler,
    });

    server.route({
        method: 'DELETE',
        path: '/pets/{id}',
        options: {
            validate: {
                params: {
                    id: Joi.required(),
                }
            },
            auth: 'simple'
        },
        handler: deletePetHandler,

    });

    server.method('getPet',getPet,{
        cache: {
            cache: 'my_cache',
            segment: 'pets',
            expiresIn: 1000 * 1000,
            generateTimeout: 2000
        }
    })

    server.logger().info('Logging.log')

    server.log(['subsystem'], 'third way for accessing it')

    await server.start();
    console.log('Server running at:', server.info.uri);
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

// exports.init = async () => {

//     await server.initialize();
//     return server;
// };

// exports.start = async () => {

//     await server.start();
//     console.log(`Server running at: ${server.info.uri}`);
//     return server;
// };

// process.on('unhandledRejection', (err) => {

//     console.log(err);
//     process.exit(1);
// });
