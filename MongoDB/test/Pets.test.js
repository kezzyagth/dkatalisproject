const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../Pets');
const btoa = require('btoa');
const auth = btoa("desi:secret");

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('Get all pets', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/pets',
            headers: {
                Authorization: `Basic ${auth}`
            },
        });
        expect(res.statusCode).to.equal(404);
    });

    it('Get pet by id', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/pets/{id}',
            headers: {
                Authorization: `Basic ${auth}`
            },
        });
        expect(res.statusCode).to.equal(404);
    });

    it('Get pet by query', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/pets/query',
            headers: {
                Authorization: `Basic ${auth}`
            },
        });
        expect(res.statusCode).to.equal(404);
    });

    it('Register a pet', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/pets',
            headers: {
                Authorization: `Basic ${auth}`
            },
        });
        expect(res.statusCode).to.equal(404);
    });

    it('Update a pet', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/pets/{id}',
            headers: {
                Authorization: `Basic ${auth}`
            },
        });
        expect(res.statusCode).to.equal(404);
    });

    it('Delete a pet', async () => {
        const res = await server.inject({
            method: 'delete',
            url: '/pets/{id}',
            headers: {
                Authorization: `Basic ${auth}`
            },
        });
        expect(res.statusCode).to.equal(404);
    });
})
