const http = require('http');
const url = require('url');
let server = http.createServer(function (request, response) {
    if (request.method === 'GET') {
        let parsedURL = url.parse(request.url, true);
        let path = parsedURL.pathname;
        var date = new Date(parsedURL.query.iso);
        if (path === '/api/parsetime') {
            var output = {
                hour: date.getUTCHours(),
                minute: date.getUTCMinutes(),
                second: date.getUTCSeconds()
            };
        }
        if (output) {
            response.writeHead(200, { 'content-type': 'application/json' });
            response.end(JSON.stringify(output));
        }else{
            response.write('the URL link is invalid')
            
        }
    }
});
server.listen(8080);