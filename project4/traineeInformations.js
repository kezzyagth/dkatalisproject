const http = require('http');
const url = require('url');
const person = require('../project2/profileClass')

http.createServer(function(request, response){
    const eci = new person.trainer ('Eci','Suci','F','20',['nursing'],'EciHouse'); //place the object on the outside so the others can use the object too
    const shanon = new person.trainee ('shanon','athooya','F','18',['coding'],'rumahShanon');
    let parsedUrl = url.parse(request.url, true);
    let path = parsedUrl.pathname;
    let output
    if (path === '/trainer') {
         output = eci
    }else if (path === '/trainee') {
         output = shanon
    }
    if (output) {
        response.writeHead(200, { 'content-type': 'application/json' });
        response.end(JSON.stringify(output));
    }else{
        response.write('the URL link is invalid')
    }
}).listen(8080);