const http = require('http') 
const url = require('url');

http.createServer(function(request, response){
    response.writeHead(200, {'content-type':'text/plan'})
    let parseurl = url.parse(request.url, true)
    response.write(`Hi, I am ${parseurl.pathname.slice(1)} `)
    response.end()
}).listen(8080);