const axios= require ('axios')
const url= ('https://xkcd.com/info.0.json')
const fs= require('fs')
const path= require('path')
axios.get(url)
.then(async response=>{
    const res = await axios({url:response.data.img, method: 'GET', responseType: 'stream'})
    const img= fs.createWriteStream(path.basename(response.data.img))
        res.data.pipe(img)
        return new Promise((resolve, reject)=>{
            img.on('finish', resolve)
            img.on('error', reject)
        })
    })