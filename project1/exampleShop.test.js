var text = require('./exampleShop')

let counter = text()

test('can we get Increment to be', () => {
    let temp = counter.getCurrentValue()
    counter.increment()
    expect(counter.getCurrentValue()).toBe(temp+1)
} ) 

test('can we get Decrement to be', () => {
    let temp = counter.getCurrentValue()
    counter.decrement()
    expect(counter.getCurrentValue()).toBe(temp-1)
} ) 

/* test('can we get anotherCounter to be', () => {
    expect(anotherCounter.getCurrentValue()).toBe(0)
} ) 
 */