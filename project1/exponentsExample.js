
//function exponents
function exponents(number1, number2){
   return Math.pow(number1, number2)
};

//console.log(Math.pow(7, 3)); 

//function square
function square(number){
    return Math.pow(number, 2)
};

// function cube()
function cube(numbers){
    return Math.pow(numbers, 3)
};

module.exports = {cube, exponents,square};
// //instructor methods

// expo(2,2)
// expo(2,3)

// const square = function(a) {
//     return expo(a,2)
// }

// const cube = function(a) {
//     return expo(a,3)
// }
