var city = ['jakarta','bandung','bogor']
var text = require('./mapExample')


function lenghtLetter(city){
    return city.map(c => c.length)
}
console.log(city.sort((a,b) => a.length - b.length)) //ascending-descending

function bigLetter(city){
    return city.map(c => c.length).filter(c => c>6).length
}
console.log(bigLetter(city))

function reduce(city){
    return city.map(c => c.length).reduce((a,b) => a+b)
}
console.log(reduce(city))

// const reducer = (accumulator, currentValue) => accumulator + currentValue;
// console.log(city.reduce(reducer))



