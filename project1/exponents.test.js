const fruits = ['apple','durian','banana']

const lengths = fruits.map(el => el.length)
const lengthsgt5 = fruits.map(el => el.length).length

test('can we test array equality', () => {
    expect(lengths).toEqual([5,6,6])
})

test('can we test array to be', () => {
    expect(lengthsgt5).toBe(3)
})

