const counter = function() {
    let currentValue = 0

    const increment = function(){
        currentValue += 1
    }
    const decrement = function(){
        currentValue -= 1
    }
    const getCurrentValue = function() {
        return currentValue
    }
    return {increment, decrement, getCurrentValue}
}

/* let myCounter = counter() //counter as an object
console.log(myCounter.getCurrentValue())
myCounter.increment() //counter.up
myCounter.increment()
console.log(myCounter.getCurrentValue())
myCounter.decrement() //counter.down
console.log(myCounter.getCurrentValue())

let anotherCounter = counter() */
// console.log(anotherCounter.getCurrentValue())
// console.log(myCounter.getCurrentValue())
// console.log(myCounter.currentValue) //currentValue tidak ada karena tidak didefinisikan di return

module.exports = counter