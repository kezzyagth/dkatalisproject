
var city = ['jakarta','bandung','bogor']


function lenghtLetter(city){
    return city.map(c => c.length)
}
console.log(city.sort((a,b) => a.length - b.length)) //ascending-descending

function bigLetter(city){

    return city.map(c => c.length).filter(c => c>6).length
}
console.log(bigLetter(city))

function reduce(city){
    return city.map(c => c.length).reduce((a,b) => a+b)
}
console.log(reduce(city))

module.exports = {lenghtLetter, bigLetter, reduce};
