// //object trainee
// function trainee(firstname,lastname,age,gender,interest){
//     this.firstname = firstname
//     this.lastname = lastname
//     this.age = age
//     this.gender = gender
//     this.interest = interest    
// }

// //trainee profile
// let shanon = new trainee('shanon','athooya','18','F','coding')
// console.log(shanon)

class person {
    constructor(first,last,gender,age,address){
        this.name = { 
             first, //already declared above so do not write twice
             last
        }
        this.age = age
        this.gender = gender
        this.address = address
    }
    greetings(){
        return `Hello i am ${this.name.first} from ${this.address}` //use this kind of ``
    }
}

class trainee extends person{
    constructor(first,last,gender,age,interest,address){
        super(first,last,gender,age,address);
        this.interest = interest
    }
    greetings(){
        return `${super.greetings()} my interest is ${this.interest}` //this is called overRiding
    }
}

class trainer extends person{
    constructor(first,last,gender,age,speciality,address){
        super(first,last,gender,age,address);
        this.speciality = speciality
    }
    greetings(){
        return `${super.greetings()} my speciality is ${this.speciality}`
    }
}

module.exports = {trainee,trainer};

