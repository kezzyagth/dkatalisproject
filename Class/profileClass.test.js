var profile = require ('./profileClass');

const eci = new profile.trainer ('Eci','Suci','F','20',['nursing'],'EciHouse'); //place the object on the outside so the others can use the object too
const shanon = new profile.trainee ('shanon','athooya','F','18',['coding'],'rumahShanon');

test('can we expect trainee profile to equal',() =>{   
    expect(shanon).toEqual({
        name: { first: 'shanon', last: 'athooya' },
        age: '18',
        gender: 'F',
        address: 'rumahShanon',
        interest: [ 'coding' ]
    })
})

test('can we expect trainer profile to equal',() => {   
    expect(eci).toEqual({
        name: { first: 'Eci', last: 'Suci' },
        age: '20',
        gender: 'F',
        address: 'EciHouse',
        speciality: [ 'nursing' ]
    })
})

test('can we expect trainee greetings to equal',() => {
    expect(shanon.greetings()).toEqual('Hello i am shanon from rumahShanon my interest is coding')
})

test('can we expect trainer greetings to equal',() => {
    expect(eci.greetings()).toEqual('Hello i am Eci from EciHouse my speciality is nursing')
})


module.exports = {eci,shanon};
console.log(shanon.greetings())









