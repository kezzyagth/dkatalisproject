const fs = require('fs');
const { Transform } = require('stream');

const rs = fs.createReadStream('./test.txt') //create stream to read data
const ws = fs.createWriteStream('./test_fix.txt')  //writestream :create stream to write data
​
String.prototype.capitalize = function () {
    return this.split("\. ").map(x => x.charAt(0).toUpperCase() + x.slice(1)).join('\. ') + "\n"
}

const capitalize = new Transform({
    transform(chunk, encoding, callback){
        this.push(chunk.toString().capitalize())
        callback() // same as continue, simply saying "I'm done with the job"
    }
})
​
rs.pipe(capitalize).pipe(ws)

/* ['ini kalimat 1', 'ini kalimat 2', 'ini kalimat 3']

'ini kalimat 1'

x.charAt(0).toUpperCase() => I
x.slice(1) => ni kalimat 1
x.slice(1)).join('\. ') => ni kalimat 1.  */